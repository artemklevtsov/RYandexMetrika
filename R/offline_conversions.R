#' @title Information about the upload of offline conversions
#'
#' @description
#' Returns information about the specified upload of offline conversions.
#'
#' @param counter_id Counter ID.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/offline_conversion/findall-docpage/>
#'
#' @export
#'
#' @family ym_offline_conversions
#'
ym_offline_conversions_list <- function(counter_id = getOption("ym.counter_id")) {
  # Check args
  assert_number(counter_id)

  path <- paste("management/v1/counter", counter_id, "offline_conversions/uploadings", sep = "/")
  api_get(path = path)$uploadings
}


#' @title Information about the upload of offline conversions
#'
#' @description
#' Returns information about the specified upload of offline conversions.
#'
#' @param counter_id Counter ID.
#' @param upload_id ID of the upload of offline conversions.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/offline_conversion/findbyid-docpage/>
#'
#' @export
#'
#' @family ym_offline_conversions
#'
ym_offline_conversions_info <- function(counter_id = getOption("ym.counter_id"), upload_id) {
  # Check args
  assert_number(counter_id)
  assert_number(upload_id)

  path <- paste("management/v1/counter", counter_id, "offline_conversions/uploading", upload_id, sep = "/")
  api_get(path = path)$uploading
}


#' @title Uploading offline conversions
#'
#' @description
#' Uploads information about offline conversions.
#'
#' @param counter_id Counter ID.
#' @param client_id_type Type of user identifiers. Allowed values: `CLIENT_ID`,
#' `USER_ID`.
#' @param file File in CSV format.
#' @param comment Comments.
#'
#' @details
#' `file` should be in CSV format, with the first row containing the column
#' headers.
#'
#' Required columns:
#' - `UserId` — Site user ID assigned by the site owner (only for client_id_type = USER_ID)
#' - `ClientId` — Site user ID assigned by Yandex.Metrica (only for client_id_type = CLIENT_ID)
#' - `Target` — Target ID.
#' - `DateTime` — Unix timestamp of the date and time of the conversion.
#'
#' Optional columns:
#' - `Price` — Goal price (use a dot as the decimal separator).
#' - `Currency` — Currency in three-letter ISO 4217 format.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/offline_conversion/upload-docpage/>
#'
#' <https://yandex.com/support/metrica/data/offline-conversion.html>
#'
#' @export
#'
#' @family ym_offline_conversions
#'
ym_offline_conversions_upload <- function(counter_id = getOption("ym.counter_id"), client_id_type, file, comment = NULL) {
  # Check args
  assert_number(counter_id)
  assert_choice(client_id_type, c("CLIENT_ID", "USER_ID"))
  assert_string(comment, null.ok = TRUE)
  assert_string(file)
  assert_file_exists(file, access = "r", extension = "csv")

  path <- paste("management/v1/counter", counter_id, "offline_conversions/upload", sep = "/")
  query <- list(
    client_id_type = client_id_type,
    comment = comment
  )
  body <- list(
    file = upload(file, type = "text/csv")
  )
  api_upload(path = path, query = query, file = file, type = "text/tsv")$uploading
}


#' @title Enabling the extended conversion window
#'
#' @description
#' Enables the extended conversion tracking window for the specified counter.
#'
#' @param counter_id Counter ID.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/offline_conversion/enableextendedthreshold-docpage/>
#'
#' @export
#'
#' @family ym_offline_conversions
#'
ym_offline_conversions_extended_window_on <- function(counter_id = getOption("ym.counter_id")) {
  # Check args
  assert_number(counter_id)

  path <- paste("management/v1/counter", counter_id, "offline_conversions/extended_threshold", sep = "/")
  api_post(path = path)$success
}

#' @title Disabling the extended conversion window
#'
#' @description
#' Disables the extended conversion window for this counter.
#'
#' @param counter_id Counter ID.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/offline_conversion/disableextendedthreshold-docpage/>
#'
#' @export
#'
#' @family ym_offline_conversions
#'
ym_offline_conversions_extended_window_off <- function(counter_id = getOption("ym.counter_id")) {
  # Check args
  assert_number(counter_id)

  path <- paste("management/v1/counter", counter_id, "offline_conversions/extended_threshold", sep = "/")
  api_del(path = path)$success
}
