#' @title List of available counters
#'
#' @description
#' Returns a list of existing counters available to user.
#'
#' @param favorite Filter by counters, which were added to Favorites.
#' @param field One or more additional parameters for the object to return.
#' @param label_id Tag filter.
#' @param offset Serial number of counter, from which begins output of list of
#' counters. First counter has the number 1.
#' @param per_page Number of counters that you want to receive.
#' @param permission Filter by level of access to counter. Parameter can contain
#'  several values, separated by commas. Allowed values: `own`, `view`, `edit`.
#' @param reverse Display counters in ascending or descending order.
#' @param search_string Filter by string. Counters, ID, name, site or mirrors which contain specified substring will be shown.
#' @param sort Sorting. Allowed values: `Uniques`, `Hits`, `Visits`, `Default`, `None`, `Name`.
#' @param status Filter by status of counter. Enabled by default. Allowed values:
#' `Active`, `Deleted`.
#' @param type 	Filter by counter type. Allowed values: `partner`, `simple`.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/counters/counters-docpage/>
#'
#' @import checkmate
#'
#' @export
#'
#' @family ym_counter
#'
ym_counter_list <- function(favorite = NULL,
                            field = NULL,
                            label_id = NULL,
                            offset = 1L,
                            per_page = NULL,
                            permission = NULL,
                            reverse = NULL,
                            search_string = NULL,
                            sort = NULL,
                            status = NULL,
                            type = NULL) {
  # Check args
  assert_flag(favorite, null.ok = TRUE)
  assert_subset(field, c("goals", "mirrors", "grants", "filters", "operations"), empty.ok = TRUE)
  assert_number(label_id, null.ok = TRUE)
  assert_count(offset, null.ok = TRUE)
  assert_count(per_page, null.ok = TRUE)
  assert_subset(permission, c("own", "view", "edit"), empty.ok = TRUE)
  assert_flag(reverse, null.ok = TRUE)
  assert_string(search_string, null.ok = TRUE)
  assert_choice(sort, c("Uniques", "Hits", "Visits", "Default", "None", "Name"), null.ok = TRUE)
  assert_choice(status, c("Active", "Deleted"), null.ok = TRUE)
  assert_choice(type, c("partner", "simple"), null.ok = TRUE)

  if (!is.null(field)) {
    field <- paste(trimws(field), collapse = ",")
  }

  if (!is.null(permission)) {
    permission <- paste(trimws(permission), collapse = ",")
  }

  path <- "management/v1/counters"
  query <- list(
    favorite = favorite,
    field = field,
    label_id = label_id,
    offset = offset,
    per_page = per_page,
    permission = permission,
    reverse = reverse,
    search_string = search_string,
    sort = sort,
    status = status,
    type = type
  )

  api_get(path = path, query = query)$counters
}


#' @title Information on counter
#'
#' @description
#' Returns information about specified counter.
#'
#' @param counter_id Counter ID.
#' @param field One or more additional parameters for the object to return.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/counters/counter-docpage/>
#'
#' @export
#'
#' @family ym_counter
#'
ym_counter_info <- function(counter_id = getOption("ym.counter_id"), field = NULL) {
  # Check args
  assert_number(counter_id)
  assert_subset(field, c("goals", "mirrors", "grants", "filters", "operation"), empty.ok = TRUE)

  if (!is.null(field)) {
    field <- paste(trimws(field), collapse = ",")
  }

  path <- paste("management/v1/counter", counter_id, sep = "/")
  query <- list(field = field)

  api_get(path = path, query = query)$counter
}


#' @title Create counter
#'
#' @description
#' Creates counter with specified parameters.
#'
#' @param name Name of counter.
#' @param site Full site domain.
#' @param mirrors List of site mirrors (domains).
#' @param goals List of structures with information on counter goals.
#' @param filters List of structures with information on counter filters.
#' @param operations List of structures with information about counter actions.
#' @param grants List of structures with information on access rights to counter.
#' @param labels List of structures with information about tags.
#' @param webvisor_options Web Visor options. Use [webvisor_options()] to generate it.
#' @param code_options Counter code options. Use [code_options()] to generate it.
#' @param monitoring_options Counter monitoring options. Use [monitoring_options()] to generate it.
#' @param create_time Date and time when counter was created.
#' @param time_zone_name Time zone for calculation of statistics.
#' @param time_zone_offset Current time zone offset from Greenwich, minutes.
#'  The ranges are specified by days of the week from Monday to Sunday. If the
#'  range is not specified for a particular day, SMS-notifications will not be
#'  sent on that day.
#' @param filter_robots Robot filtering. Possible values: 0 — factor in sessions
#'  of all robots; 1 — only filter robots by strict rules (by default);
#'  2 — filter robots by strict rules and by behavior.
#' @param visit_threshold Session timeout in seconds.
#' @param offline_options Counter offline options. Use [offline_options()] to generate it.
#' @param field One or several additional parameters of a returned object.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/counters/addcounter-docpage/>
#'
#' @export
#'
#' @family ym_counter
#'
#' @examples
#' \dontrun{
#' ym_counter_add(
#'   name = "My Counter",
#'   site = "example.com"
#' )
#' }
#'
ym_counter_add <- function(name,
                           site,
                           mirrors = NULL,
                           goals = NULL,
                           filters = NULL,
                           operations = NULL,
                           grants = NULL,
                           labels = NULL,
                           webvisor_options = NULL,
                           code_options = NULL,
                           monitoring_options = NULL,
                           create_time = NULL,
                           time_zone_name = NULL,
                           time_zone_offset = NULL,
                           filter_robots = NULL,
                           visit_threshold = NULL,
                           offline_options = NULL,
                           field = NULL) {
  # Check args
  assert_string(name)
  assert_string(site)
  assert_character(mirrors, null.ok = TRUE)

  if (!is.null(goals)) assert_goals(goals)
  if (!is.null(filters)) assert_filters(filters)
  if (!is.null(operations)) assert_operations(operations)
  if (!is.null(grants)) assert_grants(grants)
  if (!is.null(labels)) assert_labels(labels)

  assert_posixct(create_time, null.ok = TRUE)
  assert_string(time_zone_name, null.ok = TRUE)
  assert_string(time_zone_offset, pattern = "[0-9+-]", null.ok = TRUE)
  assert_choice(filter_robots, c(0, 1, 2), null.ok = TRUE)
  assert_count(visit_threshold, positive = TRUE, null.ok = TRUE)
  assert_subset(field, c("goals", "mirrors", "grants", "filters", "operations"), empty.ok = TRUE)

  if (is.null(mirrors)) {
    mirrors <- ""
  }
  if (!is.null(field)) {
    field <- paste(trimws(field), collapse = ",")
  }
  if (!is.null(create_time)) {
    create_time <- format(create_time, "%Y-%m-%dT%H:%M:%S%z")
  }

  path <- "management/v1/counters"

  query <- list(field = field)
  body <- list(
    counter = list(
      name = name,
      site2 = list(site = site),
      mirrors2 = data.table(site = mirrors),
      goals = goals,
      filters = filters,
      operations = operations,
      grants = grants,
      labels = labels,
      webvisor = webvisor_options,
      code_options = code_options,
      create_time = create_time,
      time_zone_name = time_zone_name,
      time_zone_offset = time_zone_offset,
      monitoring = monitoring_options,
      filter_robots = filter_robots,
      visit_threshold = visit_threshold,
      offline_options = offline_options
    )
  )

  to_drop <- lengths(body$counter) == 0L
  body$counter[to_drop] <- NULL

  api_post(path = path, query = query, body = body)
}


#' @title Changing counter
#'
#' @description
#' Changes data for specified counter.
#'
#' @param counter_id Counter ID.
#' @param name Name of counter.
#' @param site Full site domain.
#' @param mirrors List of site mirrors (domains).
#' @param goals List of structures with information on counter goals.
#' @param filters List of structures with information on counter filters.
#' @param operations List of structures with information about counter actions.
#' @param grants List of structures with information on access rights to counter.
#' @param labels List of structures with information about tags.
#' @param webvisor_options Web Visor options. Use [webvisor_options()] to generate it.
#' @param code_options Counter code options. Use [code_options()] to generate it.
#' @param monitoring_options Counter monitoring options. Use [monitoring_options()] to generate it.
#' @param create_time Date and time when counter was created.
#' @param time_zone_name Time zone for calculation of statistics.
#' @param time_zone_offset Current time zone offset from Greenwich, minutes.
#'  The ranges are specified by days of the week from Monday to Sunday. If the
#'  range is not specified for a particular day, SMS-notifications will not be
#'  sent on that day.
#' @param filter_robots Robot filtering. Possible values: 0 — factor in sessions
#'  of all robots; 1 — only filter robots by strict rules (by default);
#'  2 — filter robots by strict rules and by behavior.
#' @param visit_threshold Session timeout in seconds.
#' @param offline_options Counter offline options. Use [offline_options()] to generate it.
#' @param mirrors_remove Do mirrors need to be removed.
#' @param goals_remove Does the goal need removed.
#' @param filters_remove Do filters need to be deleted.
#' @param operations_remove Do actions need to be deleted.
#' @param grants_remove Do the access settings need to be deleted.
#' @param field One or several additional parameters of a returned object.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/counters/editcounter-docpage/>
#'
#' @import checkmate
#'
#' @export
#'
#' @family ym_counter
#'
ym_counter_edit <- function(counter_id,
                            name = NULL,
                            site = NULL,
                            mirrors = NULL,
                            goals = NULL,
                            filters = NULL,
                            operations = NULL,
                            grants = NULL,
                            labels = NULL,
                            webvisor_options = NULL,
                            code_options = NULL,
                            monitoring_options = NULL,
                            create_time = Sys.time(),
                            time_zone_name = NULL,
                            time_zone_offset = NULL,
                            filter_robots = NULL,
                            visit_threshold = NULL,
                            offline_options = NULL,
                            mirrors_remove = NULL,
                            goals_remove = NULL,
                            filters_remove = NULL,
                            operations_remove = NULL,
                            grants_remove = NULL,
                            field = NULL) {
  # Check args
  assert_number(counter_id)
  assert_string(name, null.ok = TRUE)
  assert_string(site, null.ok = TRUE)
  assert_character(mirrors, null.ok = TRUE)

  if (!is.null(goals)) assert_goals(goals)
  if (!is.null(filters)) assert_filters(filters)
  if (!is.null(operations)) assert_operations(operations)
  if (!is.null(grants)) assert_grants(grants)
  if (!is.null(labels)) assert_labels(labels)

  assert_posixct(create_time, null.ok = TRUE)
  assert_string(time_zone_name, null.ok = TRUE)
  assert_string(time_zone_offset, pattern = "[0-9+-]", null.ok = TRUE)
  assert_choice(filter_robots, c(0, 1, 2), null.ok = TRUE)
  assert_count(visit_threshold, positive = TRUE, null.ok = TRUE)
  assert_subset(field, c("goals", "mirrors", "grants", "filters", "operations"), empty.ok = TRUE)

  if (is.null(mirrors)) {
    mirrors <- ""
  }
  if (!is.null(field)) {
    field <- paste(trimws(field), collapse = ",")
  }
  if (!is.null(create_time)) {
    create_time <- format(create_time, "%Y-%m-%dT%H:%M:%S%z")
  }

  path <- "management/v1/counters"

  query <- list(field = field)
  body <- list(
    counter = list(
      name = name,
      site2 = list(site = site),
      mirrors2 = data.table(site = mirrors),
      goals = goals,
      filters = filters,
      operations = operations,
      grants = grants,
      labels = labels,
      webvisor = webvisor_options,
      code_options = code_options,
      create_time = create_time,
      time_zone_name = time_zone_name,
      time_zone_offset = time_zone_offset,
      monitoring = monitoring_options,
      filter_robots = filter_robots,
      visit_threshold = visit_threshold,
      offline_options = offline_options
    )
  )

  to_drop <- lengths(body$counter) == 0L
  body$counter[to_drop] <- NULL

  api_put(path = path, query = query, body = body)
}


#' @title Deleting counter
#'
#' @description
#' Deletes specified counter.
#'
#' @param counter_id Counter ID.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/counters/deletecounter-docpage/>
#'
#' @export
#'
#' @family ym_counter
#'
ym_counter_del <- function(counter_id) {
  # Check args
  assert_number(counter_id)

  path <- paste("management/v1/counter", counter_id, sep = "/")
  api_del(path = path)$success
}


#' @title Restoring counter
#'
#' @description
#' Restores a deleted counter.
#'
#' @param counter_id Counter ID.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/counters/undeletecounter-docpage/>
#'
#' @export
#'
#' @family ym_counter
#'
ym_counter_restore <- function(counter_id) {
  # Check args
  assert_number(counter_id)

  path <- paste("management/v1/counter", counter_id, "undelete", sep = "/")
  api_post(path = path)$success
}


#' @title Web Visor counter options
#'
#' @description
#' Construct Web Visor options.
#'
#' @param urls List of pages for saving to Session Replay.
#' @param arch_enabled Saving pages of site. Possible values: `load`, `html`, `none`.
#' @param arch_type Record of page content.
#' @param load_player_type Loading pages in the player. Possible values: `proxy`, `on_your_behalf`.
#' @param version Session Replay version.
#'
#' @return list with options.
#'
#' @seealso
#' ym_counter_add
#'
#' @export
#'
webvisor_options <- function(urls = NULL,
                             arch_enabled = NULL,
                             arch_type = NULL,
                             load_player_type = NULL,
                             version = NULL) {
  args <- as.list(match.call()[-1L])
  if (all(lengths(args)) == 0L) return(NULL)

  # Web Visor
  assert_character(urls, null.ok = TRUE)
  assert_flag(arch_enabled, null.ok = TRUE)
  assert_choice(arch_type, c("load", "html", "none"), null.ok = TRUE)
  assert_choice(load_player_type, c("proxy", "on_your_behalf"), null.ok = TRUE)
  assert_int(version, null.ok = TRUE)
  res <- list(
    urls = urls,
    arch_enabled = arch_enabled,
    arch_type = arch_type,
    load_player_type = load_player_type,
    webvisor_version = version
  )

  return(res)
}

#' @title Counter code options
#'
#' @description
#' Construct Counter code options.
#'
#' @param async Asynchronous code of counter.
#' @param informer_enabled Permission for display of informer.
#' @param informer_type Informer type. Possible values: ext — advanced,
#' simple — simple.
#' @param informer_size Size of informer. Possible values: 1 - size `80x15`,
#' 2 - size `80x31`, 3 - size `88x31`.
#' @param informer_indicator Parameter which will be displayed on the informer.
#' @param informer_color_start Beginning (top) color of informer in the
#' format RRGGBBAA. RR, GG, BB are saturated red, green and blue colors. The
#' level of saturation for every color is assigned a value ranging from 00 to
#' FF. AA is transparency from 00 (transparent) to FF (opaque).
#' @param informer_color_end Ending (lower) color of informer in the format
#'  RRGGBBAA. The parameter is intended for creating background gradient. Color
#'  saturation and transparency are analogously given the parameter color_start.
#' @param informer_color_text Text color on informer.
#' @param informer_color_arrow Arrow color on informer.
#' @param visor Recording and analysis of site user behavior.
#' @param ut Stop automatic page indexing.
#' @param track_hash Hash tracking in browser address bar. Option applies to AJAX sites.
#' @param xml_site For XML-sites. The noscript element should not be used in XML documents.
#' @param clickmap Gathering statistics for the click map report.
#' @param in_one_line Remove counter code in one string.
#' @param ecommerce Ecommerce flag.
#'
#' @return list with options.
#'
#' @seealso
#' ym_counter_add
#'
#' @export
#'
code_options <- function(async = NULL,
                         informer_enabled = NULL,
                         informer_type = NULL,
                         informer_size = NULL,
                         informer_indicator = NULL,
                         informer_color_start = NULL,
                         informer_color_end = NULL,
                         informer_color_text = NULL,
                         informer_color_arrow = NULL,
                         visor = NULL,
                         ut = NULL,
                         track_hash = NULL,
                         xml_site = NULL,
                         clickmap = NULL,
                         in_one_line = NULL,
                         ecommerce = NULL) {
  args <- as.list(match.call()[-1L])
  if (all(lengths(args)) == 0L) return(NULL)

  # Cehck args
  assert_flag(async, null.ok = TRUE)
  assert_flag(informer_enabled, null.ok = TRUE)
  assert_choice(informer_type, c("ext", "simple"), null.ok = TRUE)
  assert_choice(informer_size, c(1, 2, 3), null.ok = TRUE)
  assert_choice(informer_indicator, c("visits", "pageviews", "uniques"), null.ok = TRUE)
  assert_string(informer_color_start, pattern = "[A-F0-9.]", min.chars = 8L, null.ok = TRUE)
  assert_string(informer_color_end, pattern = "[A-F0-9.]", min.chars = 8L, null.ok = TRUE)
  assert_choice(informer_color_text, c(0, 1), null.ok = TRUE)
  assert_choice(informer_color_arrow, c(0, 1), null.ok = TRUE)
  assert_flag(visor, null.ok = TRUE)
  assert_flag(ut, null.ok = TRUE)
  assert_flag(track_hash, null.ok = TRUE)
  assert_flag(xml_site, null.ok = TRUE)
  assert_flag(clickmap, null.ok = TRUE)
  assert_flag(in_one_line, null.ok = TRUE)
  assert_flag(ecommerce, null.ok = TRUE)

  res <- list(
    async = async,
    informer = list(
      enabled = informer_enabled,
      type = informer_type,
      size = informer_size,
      indicator = informer_indicator,
      color_start = informer_color_start,
      color_end = informer_color_end,
      color_text = informer_color_text,
      color_arrow = informer_color_arrow
    ),
    visor = visor,
    ut = ut,
    track_hash = track_hash,
    xml_site = xml_site,
    clickmap = clickmap,
    in_one_line = in_one_line,
    ecommerce = ecommerce
  )

  return(res)
}

#' @title Counter monitoring options
#'
#' @description
#' Contruct monitoring options.
#'
#' @param enable Notifications for monitoring site availability.
#' @param emails List of email addresses for getting site monitoring notifications.
#' @param enable_sms SMS-notifications for monitoring site availability.
#' @param sms_time Permitted time ranges for sending SMS-notifications.
#' @param phones List of telephone numbers for getting site monitoring notifications.
#'
#' @return list with options.
#'
#' @seealso
#' ym_counter_add
#'
#' @export
#'
monitoring_options <- function(enable = NULL,
                               emails = NULL,
                               enable_sms = NULL,
                               sms_time = NULL,
                               phones = NULL) {
  args <- as.list(match.call()[-1L])
  if (all(lengths(args)) == 0L) return(NULL)

  # Check args
  assert_flag(enable, null.ok = TRUE)
  assert_flag(enable_sms, null.ok = TRUE)
  assert_string(sms_time, pattern = "[0-9;,-]+", null.ok = TRUE)
  assert_character(emails, pattern = "^[[:alnum:]._-]+@[[:alnum:].-]+$", null.ok = TRUE)
  assert_character(phones, pattern = "[0-9]+", null.ok = TRUE)

  if (!is.null(enable)) {
    enable <- as.integer(enable)
  }
  if (!is.null(enable_sms)) {
    enable_sms <- as.integer(enable_sms)
  }
  if (!is.null(emails)) {
    emails <- paste(emails, collapse = ",")
  }
  if (!is.null(phones)) {
    phones <- paste(phones, collapse = ",")
  }

  list(
    enable_monitoring = enable,
    emails = emails,
    enable_sms = enable_sms,
    sms_time = sms_time,
    phones = phones
  )
}


#' @title Counter offline options
#'
#' @param conversion_extended_threshold Extended period for tracking offline conversions.
#' @param calls_extended_threshold Extended call tracking period.
#' @param visits_extended_threshold Extended tracking period for offline visits.
#'
#' @return list with options.
#'
#' @seealso
#' ym_counter_add
#'
#' @export
#'
offline_options <- function(conversion_extended_threshold = NULL,
                            calls_extended_threshold = NULL,
                            visits_extended_threshold = NULL) {
  args <- as.list(match.call()[-1L])
  if (all(lengths(args)) == 0L) return(NULL)

  # Check args
  assert_flag(conversion_extended_threshold, null.ok = TRUE)
  assert_flag(calls_extended_threshold, null.ok = TRUE)
  assert_flag(visits_extended_threshold, null.ok = TRUE)

  list(
    offline_conversion_extended_threshold = conversion_extended_threshold,
    offline_calls_extended_threshold = calls_extended_threshold,
    offline_visits_extended_threshold = visits_extended_threshold
  )
}
