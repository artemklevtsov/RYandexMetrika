list_clean <- function(data, fun = is.null, recursive = FALSE) {
    assert_list(data)

    if (recursive) {
        data <- lapply(data, function(item) {
            if (is.list(item)) {
                item <- list_clean(item, fun, recursive = TRUE)
            }
            return(item)
        })
    }
    idx <- vapply(data, fun, logical(1L))
    data[idx] <- NULL
    return(data)
}


unnest <- function(data, col, sep = "_") {
    assert_data_frame(data)
    assert_names(names(data), must.include = col)
    vals <- rbindlist(data[[col]])
    if (nrow(vals) != nrow(data)) {
        return(NULL)
    }
    setnames(vals, paste(col, names(vals), sep = sep))
    data[, (names(vals)) := vals]
    data[, (col) := NULL]
    return(invisible(data))
}
