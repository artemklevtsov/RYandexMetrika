#' @title Yandex.Metrika Data
#'
#' @description
#' Provides access to Yandex.Metrica statistical data, including data available in reports. Returns result in the form of a table.
#'
#' @param counter_id Counter ID.
#' @param date1 Start date of requesting data period.
#' @param date2 End date of requesting data period.
#' @param metrics List of metrics, separated by commas. Limit: 20 metrics in request.
#' @param dimensions List of dimensions, separated by commas. Limit: 10 dimensions in request.
#' @param filters List of filters. Limit: 20 filter in request.
#' @param sort A list of dimensions and metrics separated by comma, which are
#' sorted. By default it is sorted in descending order (indicated by the «-»
#' symbol in front of the dimension or metric ). To sort data in ascending
#' order, remove the «-» symbol.
#' @param lang Language.
#' @param limit Number of elements on results page. Limit: 100,000.
#' @param offset Index of first row of requesting data, beginning with 1.
#' @param preset Report template. See details in [documentation](https://tech.yandex.com/metrika/doc/api2/api_v1/presets/presets-docpage/).
#' @param timezone Time zone in ±hh:mm format within range of `[-23:59; +23:59]`;
#' this time zone is used to calculate the query selection period as well as the
#'  date- and time-specific dimensions. By default, the counter's time zone is used.
#'
#' @references
#' https://tech.yandex.com/metrika/doc/api2/api_v1/data-docpage/
#'
#' @import checkmate
#' @import data.table
#' @importFrom utils type.convert
#'
#' @export
#'
#' @family ym_report
#'
#' @examples
#' \dontrun{
#' ym_report_data(
#'   counter_id = counter_id,
#'   date1 = as.Date("2019-01-01"),
#'   date2 = as.Date("2019-02-28"),
#'   dimensions = c("ym:s:year", "ym:s:month"),
#'   metrics = c("ym:s:pageviews", "ym:s:visits")
#' )
#' }
#'
ym_report_data <- function(counter_id = getOption("ym.counter_id"),
                           date1 = Sys.Date() - 8L, date2 = Sys.Date() - 1L,
                           metrics = NULL, dimensions = NULL, sort = NULL, filters = NULL,
                           preset = NULL, timezone = NULL, lang = "en", limit = 100000L, offset = 1L) {

  # Check args
  assert_number(counter_id)
  assert_int(limit, lower = 1L, upper = 100000L)
  assert_int(offset, lower = 1L)
  assert_string(preset, null.ok = TRUE)
  assert_date(date1, upper = Sys.Date())
  assert_date(date2, upper = Sys.Date())
  assert_choice(lang, c("ru", "en"))
  assert_string(timezone, pattern = "^[+-]\\d{2}:\\d{2}", null.ok = TRUE)
  assert_character(metrics, pattern = "^ym:")
  assert_character(dimensions, pattern = "^ym:")

  # Collapse fields
  if (!is.null(metrics)) {
    metrics <- paste(metrics, collapse = ",")
  }
  if (!is.null(dimensions)) {
    dimensions <- paste(dimensions, collapse = ",")
  }
  if (!is.null(filters)) {
    filters <- paste(filters, collapse = ",")
  }

  params <- list(
    ids = counter_id,
    accuracy = "full",
    date1 = format(date1, "%F"),
    date2 = format(date2, "%F"),
    metrics = metrics,
    dimensions = dimensions,
    sort = sort,
    timezone = timezone,
    preset = preset,
    lang = lang,
    limit = limit,
    offset = offset
  )
  # Clean empty params
  params <- params[lengths(params) > 0L]

  path <- "stat/v1/data"
  res <- api_get(path, params)

  # Fetch pages
  if (res$total_rows > res$query$limit) {
    pg_resp <- api_get_pages(path, params, res$total_rows)

    tmp <- lapply(tmp, .subset2, "data")
    keys <- c("dimensions", "metrics")
    tmp <- lapply(keys, function(k) unlist(lapply(tmp, .subset2, k), recursive = FALSE))
    names(tmp) <- keys
    res$data <- tmp
  }

  # Post process dimensions
  dims <- lapply(res$data$dimensions, .subset2, "name")
  dims <- transpose(dims)
  setDT(dims)
  dims <- type.convert(dims, as.is = TRUE)

  # Postprocess metrix
  mets <- transpose(res$data$metrics)
  setDT(mets)

  # Combine dimensions and metrics
  data <- data.table(dims, mets)
  cnames <- c(res$query$dimensions, res$query$metrics)
  cnames <- gsub("^ym:\\w+:", "", cnames)
  setnames(data, cnames)

  # Convert date columns
  dt_cols <- grep("date$", cnames, ignore.case = TRUE)
  if (length(dt_cols) > 0L) {
    data[, (dt_cols) := lapply(.SD, as.Date), .SDcols = dt_cols]
  }

  # Convert date time columns
  dtm_cols <- grep("dateTime$", cnames, ignore.case = TRUE)
  if (length(dtm_cols) > 0L) {
    data[, (dtm_cols) := lapply(.SD, as.POSIXct), .SDcols = dtm_cols]
  }

  to_drop <- c("data", "min", "max", "totals")
  setattr(data, "metadata", res[!names(res) %in% to_drop])

  return(data[])
}
