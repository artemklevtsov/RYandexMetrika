#' @title List of filters
#'
#' @description
#' Returns information about counter filters.
#'
#' @param counter_id Counter ID.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/filters/filters-docpage/>
#'
#' @export
#'
#' @family ym_filter
#'
ym_filter_list <- function(counter_id = getOption("ym.counter_id")) {
  # Check args
  assert_number(counter_id)

  path <- paste("management/v1/counter", counter_id, "filters", sep = "/")
  api_get(path = path)$filters
}


#' @title Information on filter
#'
#' @description
#' Returns information about specified counter filter.
#'
#' @param counter_id Counter ID.
#' @param filter_id Filter ID.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/filters/filter-docpage/>
#'
#' @export
#'
#' @family ym_filter
#'
ym_filter_info <- function(counter_id = getOption("ym.counter_id"), filter_id) {
  # Check args
  assert_number(counter_id)
  assert_number(filter_id)

  path <- paste("management/v1/counter", counter_id, "filter", filter_id, sep = "/")
  api_get(path = path)$filter
}


#' @title Create filter
#'
#' @description
#' Creates counter filter.
#'
#' @param counter_id Counter ID.
#' @param attr Type of data, to which a filter is applied.
#' @param type Filter type or action for filter.
#' @param value Filter value.
#' @param action Filter type.
#' @param status Filter status.
#' @param with_subdomains Filter by subdomains.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/filters/addfilter-docpage/>
#'
#' @export
#'
#' @family ym_filter
#'
ym_filter_add <- function(counter_id = getOption("ym.counter_id"), attr, type, value, action, status, with_subdomains) {
  # Check args
  assert_number(counter_id)
  query <- list(
    attr = attr,
    type = type,
    value = value,
    action = action,
    status = status,
    with_subdomains = with_subdomains
  )
  assert_filter(query)

  body <- list(filter = query)

  path <- paste("management/v1/counter", counter_id, "filters", sep = "/")
  api_post(path = path, body = body)$filter
}


#' @title Changing filter
#'
#' @description
#' Changes setting of specified counter filter.
#'
#' @param counter_id Counter ID.
#' @param filter_id Filter ID.
#' @param attr Type of data, to which a filter is applied.
#' @param type Filter type or action for filter.
#' @param value Filter value.
#' @param action Filter type.
#' @param status Filter status.
#' @param with_subdomains Filter by subdomains.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/filters/editfilter-docpage/>
#'
#' @export
#'
#' @family ym_filter
#'
ym_filter_edit <- function(counter_id = getOption("ym.counter_id"), filter_id, attr, type, value, action, status, with_subdomains) {
  # Check args
  assert_number(counter_id)
  assert_number(filter_id)
  query <- list(
    attr = attr,
    type = type,
    value = value,
    action = action,
    status = status,
    with_subdomains = with_subdomains
  )
  assert_filter(query)

  body <- list(filter = query)

  path <- paste("management/v1/counter", counter_id, "filter", filter_id, sep = "/")
  api_put(path = path, body = body)$filter
}


#' @title Deleting filter
#'
#' @description
#' Deletes counter filter.
#'
#' @param counter_id Counter ID.
#' @param filter_id Filter ID.
#'
#' @references
#' <https://tech.yandex.com/metrika/doc/api2/management/filters/deletefilter-docpage/>
#'
#' @export
#'
#' @family ym_filter
#'
ym_filter_del <- function(counter_id = getOption("ym.counter_id"), filter_id) {
  # Check args
  assert_number(counter_id)
  assert_number(filter_id)

  path <- paste("management/v1/counter", counter_id, "goal", filter_id, sep = "/")
  api_del(path = path)$success
}
