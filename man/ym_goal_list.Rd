% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/goal.R
\name{ym_goal_list}
\alias{ym_goal_list}
\title{List of goals}
\usage{
ym_goal_list(counter_id = getOption("ym.counter_id"), useDeleted = FALSE)
}
\arguments{
\item{counter_id}{Counter ID.}

\item{useDeleted}{Information on deleted goals.}
}
\description{
Returns information about counter goals.
}
\references{
\url{https://tech.yandex.com/metrika/doc/api2/management/goals/goals-docpage/}
}
\seealso{
Other ym_goal: 
\code{\link{ym_goal_add}()},
\code{\link{ym_goal_del}()},
\code{\link{ym_goal_edit}()},
\code{\link{ym_goal_info}()}
}
\concept{ym_goal}
