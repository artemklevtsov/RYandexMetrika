---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
    collapse = TRUE,
    comment = "#>",
    fig.path = "man/figures/README-"
)
options(width = 120L)
options(digits = 3L)
```


[![Build status](https://gitlab.com/artemklevtsov/yametrika/badges/master/pipeline.svg)](https://gitlab.com/artemklevtsov/yametrika/commits/master)
 [![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)  [![CRAN status](https://www.r-pkg.org/badges/version/yametrika)](https://cran.r-project.org/package=yametrika)

# yametrika

## Installation

To install the stable version the following command can be used:

```r
install.packages("yametrika")
```

To install the development version the following command can be used:

```r
install.packages("yametrika", repos = "https://artemklevtsov.gitlab.io/yametrika/drat")
```

## Usage

### Prepare

1. Get debugging token as described [here](https://tech.yandex.com/oauth/doc/dg/tasks/get-oauth-token-docpage/).
2. Set up token string to the `YM_TOKEN` environment variable.

### Authorization

```{r, results = "hide"}
# Load package
library(yametrika)
# Get token from environment variable
token <- Sys.getenv("YM_TOKEN")
# Get counter ID from environment variable
counter_id <- as.numeric(Sys.getenv("YM_COUNTER"))
# Set default counter ID
options(ym.counter_id = counter_id)
# Set token
options(ym.token = token)
# Authorize app
ym_set_token()
```

### Fetch management data

```{r, eval=FALSE}
# Get couunters
ym_counter_list()
# Get goals
ym_goal_list()
# Get filter
ym_filter_list()
```

### Fetch report data

```{r}
ym_report_data(
    counter_id = counter_id,
    date1 = as.Date("2019-01-01"),
    date2 = as.Date("2019-02-28"),
    dimensions = c("ym:s:year", "ym:s:month"),
    metrics = c("ym:s:pageviews", "ym:s:visits")
)
```

```{r}
ym_report_data(
    counter_id = counter_id,
    date1 = as.Date("2019-01-01"),
    date2 = as.Date("2019-02-28"),
    dimensions = c("ym:s:deviceCategory"),
    metrics = c("ym:s:pageviews", "ym:s:visits")
)
```


## Bug reports

Use the following command to go to the page for bug report submissions:

```r
bug.report(package = "yametrika")
```

Before reporting a bug or submitting an issue, please do the following:

- Make sure that no error was found and corrected previously identified. You can use the search by the bug tracker;
- Check the news list for the current version of the package. An error it might have been caused by changes in the package. This can be done with `news(package = "yametrika", Version == packageVersion("yametrika"))` command;
- Make a minimal reproducible example of the code that consistently causes the error;
- Make sure that the error triggered in the function from the `yametrika` package, rather than in the code that you pass, that is other functions or packages;
- Try to reproduce the error with the last development version of the package from the git repository.

When submitting a bug report please include the output produced by functions `traceback()` and `sessionInfo()`. This may save a lot of time.

## License

The `yametrika` package is distributed under [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0) license.

